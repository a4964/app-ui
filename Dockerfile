FROM nginx:latest

COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

COPY ./app/index.html /usr/share/nginx/html/index.html

RUN mkdir -p /usr/share/nginx/html/css

COPY ./app/css/style.css /usr/share/nginx/html/css/style.css  