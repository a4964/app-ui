# App-UI

Pagina di frontend che permette l'invio di dati, serializzati JSON, ad un servizio.

## Build e push immagine Docker

Tramite questi comandi è possibile effettuare il build e push dell'immagine:

```
docker build -t andreasosi/microservices-project:service-frontend .

docker push andreasosi/microservices-project:service-frontend

```

